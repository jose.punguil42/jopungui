USE [master]
GO
/****** Object:  Database [Pichincha]    Script Date: 24/05/2022 9:43:34 ******/
CREATE DATABASE [Pichincha]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Pichincha', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESSB\MSSQL\DATA\Pichincha.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Pichincha_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESSB\MSSQL\DATA\Pichincha_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Pichincha] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Pichincha].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Pichincha] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Pichincha] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Pichincha] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Pichincha] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Pichincha] SET ARITHABORT OFF 
GO
ALTER DATABASE [Pichincha] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Pichincha] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Pichincha] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Pichincha] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Pichincha] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Pichincha] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Pichincha] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Pichincha] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Pichincha] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Pichincha] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Pichincha] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Pichincha] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Pichincha] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Pichincha] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Pichincha] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Pichincha] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Pichincha] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Pichincha] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Pichincha] SET  MULTI_USER 
GO
ALTER DATABASE [Pichincha] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Pichincha] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Pichincha] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Pichincha] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Pichincha] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Pichincha] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [Pichincha] SET QUERY_STORE = OFF
GO
USE [Pichincha]
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 24/05/2022 9:43:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cliente](
	[cl_id] [int] IDENTITY(1,1) NOT NULL,
	[cl_identificacion] [varchar](10) NOT NULL,
	[cl_nombres] [varchar](50) NOT NULL,
	[cl_edad] [int] NOT NULL,
	[cl_fecha_nacimiento] [datetime] NOT NULL,
	[cl_apellidos] [varchar](50) NOT NULL,
	[cl_direccion] [varchar](50) NOT NULL,
	[cl_telefono] [varchar](10) NOT NULL,
	[cl_estado_civil] [int] NOT NULL,
	[cl_identificacion_conyugue] [varchar](10) NOT NULL,
	[cl_nombre_conyugue] [varchar](50) NOT NULL,
	[cl_sujeto_credito] [char](1) NOT NULL,
	[cl_estado] [char](1) NULL,
 CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED 
(
	[cl_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ejecutivo]    Script Date: 24/05/2022 9:43:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ejecutivo](
	[ej_id] [int] IDENTITY(1,1) NOT NULL,
	[ej_identificacion] [varchar](10) NOT NULL,
	[ej_nombre] [varchar](50) NOT NULL,
	[ej_apellido] [varchar](50) NOT NULL,
	[ej_direccion] [varchar](50) NOT NULL,
	[ej_telefono] [varchar](15) NOT NULL,
	[ej_celular] [varchar](15) NOT NULL,
	[ej_edad] [int] NOT NULL,
	[ej_estado] [char](1) NULL,
	[pa_id] [int] NOT NULL,
 CONSTRAINT [PK_Ejecutivo] PRIMARY KEY CLUSTERED 
(
	[ej_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Marca]    Script Date: 24/05/2022 9:43:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Marca](
	[ma_id] [int] IDENTITY(1,1) NOT NULL,
	[ma_nombre] [varchar](50) NOT NULL,
	[ma_estado] [char](1) NULL,
 CONSTRAINT [PK_Marca] PRIMARY KEY CLUSTERED 
(
	[ma_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Patio]    Script Date: 24/05/2022 9:43:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Patio](
	[pa_id] [int] IDENTITY(1,1) NOT NULL,
	[pa_nombre] [varchar](50) NOT NULL,
	[pa_direccion] [varchar](50) NOT NULL,
	[pa_telefono] [varchar](10) NOT NULL,
	[pa_numero_venta] [int] NOT NULL,
	[pa_estado] [char](1) NULL,
 CONSTRAINT [PK_Patio] PRIMARY KEY CLUSTERED 
(
	[pa_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Solicitud_Credito]    Script Date: 24/05/2022 9:43:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Solicitud_Credito](
	[sc_id] [int] IDENTITY(1,1) NOT NULL,
	[sc_fecha_elaboracion] [datetime] NULL,
	[cl_id] [int] NOT NULL,
	[pa_id] [int] NULL,
	[ve_id] [int] NOT NULL,
	[sc_meses_plazo] [int] NOT NULL,
	[sc_cuotas] [int] NOT NULL,
	[sc_entrada] [money] NOT NULL,
	[ej_id] [int] NOT NULL,
	[sc_observacion] [varchar](250) NULL,
	[sc_estado] [int] NULL,
	[cl_fecha_asginacion] [datetime] NULL,
	[cl_asignacion] [bit] NULL,
 CONSTRAINT [PK_Solicitud_Credito] PRIMARY KEY CLUSTERED 
(
	[sc_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Vehiculo]    Script Date: 24/05/2022 9:43:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vehiculo](
	[ve_id] [int] IDENTITY(1,1) NOT NULL,
	[ve_placa] [varchar](7) NOT NULL,
	[ve_modelo] [varchar](50) NOT NULL,
	[ve_chasis] [varchar](50) NOT NULL,
	[ma_id] [int] NOT NULL,
	[ve_tipo] [int] NULL,
	[ve_cilindraje] [varchar](50) NOT NULL,
	[ve_avaluo] [money] NOT NULL,
 CONSTRAINT [PK_Vehiculo] PRIMARY KEY CLUSTERED 
(
	[ve_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Cliente] ON 

INSERT [dbo].[Cliente] ([cl_id], [cl_identificacion], [cl_nombres], [cl_edad], [cl_fecha_nacimiento], [cl_apellidos], [cl_direccion], [cl_telefono], [cl_estado_civil], [cl_identificacion_conyugue], [cl_nombre_conyugue], [cl_sujeto_credito], [cl_estado]) VALUES (1, N'1720477346', N'Carlos Javier Modificado 2', 35, CAST(N'1986-11-14T00:00:00.000' AS DateTime), N' Torres Torres', N'Casa direcci?n', N'0995691214', 1, N'1720477346', N'Caren Torres', N'A', NULL)
INSERT [dbo].[Cliente] ([cl_id], [cl_identificacion], [cl_nombres], [cl_edad], [cl_fecha_nacimiento], [cl_apellidos], [cl_direccion], [cl_telefono], [cl_estado_civil], [cl_identificacion_conyugue], [cl_nombre_conyugue], [cl_sujeto_credito], [cl_estado]) VALUES (2, N'1720477347', N'Diego German', 35, CAST(N'1986-11-14T00:00:00.000' AS DateTime), N' Paucar Punguil', N' Casa direcci?n', N'0995691214', 2, N'1720477346', N'Lorena Navarrete', N'A', NULL)
INSERT [dbo].[Cliente] ([cl_id], [cl_identificacion], [cl_nombres], [cl_edad], [cl_fecha_nacimiento], [cl_apellidos], [cl_direccion], [cl_telefono], [cl_estado_civil], [cl_identificacion_conyugue], [cl_nombre_conyugue], [cl_sujeto_credito], [cl_estado]) VALUES (3, N'1720477348', N'Mayra Gabriela', 35, CAST(N'1986-11-14T00:00:00.000' AS DateTime), N' Altamirano Carrera', N' Casa direcci?n', N'0995691214', 1, N'1720477346', N'Jose Punguil', N'A', NULL)
INSERT [dbo].[Cliente] ([cl_id], [cl_identificacion], [cl_nombres], [cl_edad], [cl_fecha_nacimiento], [cl_apellidos], [cl_direccion], [cl_telefono], [cl_estado_civil], [cl_identificacion_conyugue], [cl_nombre_conyugue], [cl_sujeto_credito], [cl_estado]) VALUES (4, N'1720477349', N'Sandra Lorena', 35, CAST(N'1986-11-14T00:00:00.000' AS DateTime), N' Carnedas Sancho', N'Casa direcci?n', N'0995691214', 3, N'1720477346', N'Santiago Zambrano', N'I', NULL)
SET IDENTITY_INSERT [dbo].[Cliente] OFF
GO
SET IDENTITY_INSERT [dbo].[Ejecutivo] ON 

INSERT [dbo].[Ejecutivo] ([ej_id], [ej_identificacion], [ej_nombre], [ej_apellido], [ej_direccion], [ej_telefono], [ej_celular], [ej_edad], [ej_estado], [pa_id]) VALUES (1, N'1723521325', N'Ejecutivo 1', N' Apellido 1', N'El inca', N'025105496', N' 0987654321', 32, NULL, 1)
INSERT [dbo].[Ejecutivo] ([ej_id], [ej_identificacion], [ej_nombre], [ej_apellido], [ej_direccion], [ej_telefono], [ej_celular], [ej_edad], [ej_estado], [pa_id]) VALUES (2, N'1723521326', N'Ejecutivo 1', N' Apellido 1', N'El inca', N'025105496', N' 0987654321', 32, NULL, 1)
INSERT [dbo].[Ejecutivo] ([ej_id], [ej_identificacion], [ej_nombre], [ej_apellido], [ej_direccion], [ej_telefono], [ej_celular], [ej_edad], [ej_estado], [pa_id]) VALUES (3, N'1723521327', N'Ejecutivo 1', N' Apellido 1', N'El inca', N'025105496', N' 0987654321', 32, NULL, 1)
INSERT [dbo].[Ejecutivo] ([ej_id], [ej_identificacion], [ej_nombre], [ej_apellido], [ej_direccion], [ej_telefono], [ej_celular], [ej_edad], [ej_estado], [pa_id]) VALUES (4, N'1723521328', N'Ejecutivo 1', N' Apellido 1', N'El inca', N'025105496', N' 0987654321', 32, NULL, 1)
INSERT [dbo].[Ejecutivo] ([ej_id], [ej_identificacion], [ej_nombre], [ej_apellido], [ej_direccion], [ej_telefono], [ej_celular], [ej_edad], [ej_estado], [pa_id]) VALUES (5, N'1723521329', N'Ejecutivo 1', N' Apellido 1', N'El inca', N'025105496', N' 0987654321', 32, NULL, 1)
SET IDENTITY_INSERT [dbo].[Ejecutivo] OFF
GO
SET IDENTITY_INSERT [dbo].[Marca] ON 

INSERT [dbo].[Marca] ([ma_id], [ma_nombre], [ma_estado]) VALUES (1, N'Chevrolet', NULL)
INSERT [dbo].[Marca] ([ma_id], [ma_nombre], [ma_estado]) VALUES (2, N'KIA', NULL)
INSERT [dbo].[Marca] ([ma_id], [ma_nombre], [ma_estado]) VALUES (3, N'Hyundai', NULL)
INSERT [dbo].[Marca] ([ma_id], [ma_nombre], [ma_estado]) VALUES (4, N'Toyota', NULL)
INSERT [dbo].[Marca] ([ma_id], [ma_nombre], [ma_estado]) VALUES (5, N'Great Wall', NULL)
INSERT [dbo].[Marca] ([ma_id], [ma_nombre], [ma_estado]) VALUES (6, N'JAC', NULL)
INSERT [dbo].[Marca] ([ma_id], [ma_nombre], [ma_estado]) VALUES (7, N'Chery', NULL)
INSERT [dbo].[Marca] ([ma_id], [ma_nombre], [ma_estado]) VALUES (8, N'Renault', NULL)
SET IDENTITY_INSERT [dbo].[Marca] OFF
GO
SET IDENTITY_INSERT [dbo].[Patio] ON 

INSERT [dbo].[Patio] ([pa_id], [pa_nombre], [pa_direccion], [pa_telefono], [pa_numero_venta], [pa_estado]) VALUES (1, N'Patio Amazonas', N'Amazons', N'024536271', 1, NULL)
INSERT [dbo].[Patio] ([pa_id], [pa_nombre], [pa_direccion], [pa_telefono], [pa_numero_venta], [pa_estado]) VALUES (2, N'Patio Solca Modificado', N'Solca', N'098763321', 2, NULL)
SET IDENTITY_INSERT [dbo].[Patio] OFF
GO
SET IDENTITY_INSERT [dbo].[Solicitud_Credito] ON 

INSERT [dbo].[Solicitud_Credito] ([sc_id], [sc_fecha_elaboracion], [cl_id], [pa_id], [ve_id], [sc_meses_plazo], [sc_cuotas], [sc_entrada], [ej_id], [sc_observacion], [sc_estado], [cl_fecha_asginacion], [cl_asignacion]) VALUES (13, CAST(N'2022-05-19T01:44:04.220' AS DateTime), 3, 0, 3, 10, 10, 4000.0000, 1, N'Solicitud tres', 1, NULL, 0)
INSERT [dbo].[Solicitud_Credito] ([sc_id], [sc_fecha_elaboracion], [cl_id], [pa_id], [ve_id], [sc_meses_plazo], [sc_cuotas], [sc_entrada], [ej_id], [sc_observacion], [sc_estado], [cl_fecha_asginacion], [cl_asignacion]) VALUES (14, CAST(N'2022-05-19T01:44:04.220' AS DateTime), 1, 1, 4, 10, 10, 4000.0000, 1, N'Solicitud cuatro', 1, CAST(N'2022-05-19T01:44:04.220' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Solicitud_Credito] OFF
GO
SET IDENTITY_INSERT [dbo].[Vehiculo] ON 

INSERT [dbo].[Vehiculo] ([ve_id], [ve_placa], [ve_modelo], [ve_chasis], [ma_id], [ve_tipo], [ve_cilindraje], [ve_avaluo]) VALUES (1, N'123ABC', N'Modelo C modificado', N'12345C', 1, 1, N'1.6', 20.0000)
INSERT [dbo].[Vehiculo] ([ve_id], [ve_placa], [ve_modelo], [ve_chasis], [ma_id], [ve_tipo], [ve_cilindraje], [ve_avaluo]) VALUES (2, N'123ABD', N'Modelo D', N'123456D', 2, 1, N'1.6', 20.0000)
INSERT [dbo].[Vehiculo] ([ve_id], [ve_placa], [ve_modelo], [ve_chasis], [ma_id], [ve_tipo], [ve_cilindraje], [ve_avaluo]) VALUES (3, N'123ABF', N'Modelo F', N'123456F', 3, 1, N'1.6', 20.0000)
INSERT [dbo].[Vehiculo] ([ve_id], [ve_placa], [ve_modelo], [ve_chasis], [ma_id], [ve_tipo], [ve_cilindraje], [ve_avaluo]) VALUES (4, N'123ABG', N'Modelo G', N'123456G', 4, 1, N'1.6', 20.0000)
INSERT [dbo].[Vehiculo] ([ve_id], [ve_placa], [ve_modelo], [ve_chasis], [ma_id], [ve_tipo], [ve_cilindraje], [ve_avaluo]) VALUES (5, N'123ABH', N'Modelo H', N'123456H', 5, 1, N'1.6', 20.0000)
INSERT [dbo].[Vehiculo] ([ve_id], [ve_placa], [ve_modelo], [ve_chasis], [ma_id], [ve_tipo], [ve_cilindraje], [ve_avaluo]) VALUES (6, N'123ABI', N'Modelo I', N'123456I', 6, 1, N'1.6', 20.0000)
INSERT [dbo].[Vehiculo] ([ve_id], [ve_placa], [ve_modelo], [ve_chasis], [ma_id], [ve_tipo], [ve_cilindraje], [ve_avaluo]) VALUES (7, N'123ABJ', N'Modelo J', N'123456J', 7, 1, N'1.6', 20.0000)
SET IDENTITY_INSERT [dbo].[Vehiculo] OFF
GO
ALTER TABLE [dbo].[Ejecutivo]  WITH CHECK ADD  CONSTRAINT [FK_Ejecutivo_Patio] FOREIGN KEY([pa_id])
REFERENCES [dbo].[Patio] ([pa_id])
GO
ALTER TABLE [dbo].[Ejecutivo] CHECK CONSTRAINT [FK_Ejecutivo_Patio]
GO
ALTER TABLE [dbo].[Solicitud_Credito]  WITH CHECK ADD  CONSTRAINT [FK_Solicitud_Credito_Cliente] FOREIGN KEY([cl_id])
REFERENCES [dbo].[Cliente] ([cl_id])
GO
ALTER TABLE [dbo].[Solicitud_Credito] CHECK CONSTRAINT [FK_Solicitud_Credito_Cliente]
GO
ALTER TABLE [dbo].[Solicitud_Credito]  WITH CHECK ADD  CONSTRAINT [FK_Solicitud_Credito_Ejecutivo] FOREIGN KEY([ej_id])
REFERENCES [dbo].[Ejecutivo] ([ej_id])
GO
ALTER TABLE [dbo].[Solicitud_Credito] CHECK CONSTRAINT [FK_Solicitud_Credito_Ejecutivo]
GO
ALTER TABLE [dbo].[Solicitud_Credito]  WITH CHECK ADD  CONSTRAINT [FK_Solicitud_Credito_Vehiculo] FOREIGN KEY([ve_id])
REFERENCES [dbo].[Vehiculo] ([ve_id])
GO
ALTER TABLE [dbo].[Solicitud_Credito] CHECK CONSTRAINT [FK_Solicitud_Credito_Vehiculo]
GO
ALTER TABLE [dbo].[Vehiculo]  WITH CHECK ADD  CONSTRAINT [FK_Vehiculo_Marca] FOREIGN KEY([ma_id])
REFERENCES [dbo].[Marca] ([ma_id])
GO
ALTER TABLE [dbo].[Vehiculo] CHECK CONSTRAINT [FK_Vehiculo_Marca]
GO
USE [master]
GO
ALTER DATABASE [Pichincha] SET  READ_WRITE 
GO
