﻿using System;
using System.Collections.Generic;
using System.Text;

namespace nombremicroservicio.Entities.Utilities
{
    public static class Enumeradores
    {
        public enum EstadoSolicitudCredito
        {
            Registrada = 1,
            Despachada = 2,
            Cancelada = 3,
        }

        public enum CodigoError
        {
            SolicitudIngresada = 333,
            VehiculoReservado = 334,
            ErrorGenerico = 999,
            ok = 100,
        }

        public enum EstadoAsignacionClientePatio
        {
            Activo = 1,
            Inactivo = 2
        }
    }
}
