﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace nombremicroservicio.Entities.Dto
{
    public class SolicitudCreditoDto
    {
        [Required]
        public DateTime? ScFechaElaboracion { get; set; }

        [Required]
        public string ScIdentificacion { get; set; }

        [Required]
        public int? PaId { get; set; }

        [Required]
        public string ScPlaca { get; set; }

        [Required]
        public int ScMesesPlazo { get; set; }

        [Required]
        public int ScCuotas { get; set; }

        [Required]
        public decimal ScEntrada { get; set; }

        [Required]
        public int EjId { get; set; }
        public string ScObservacion { get; set; }
        public int? ScEstado { get; set; }
    }
}
