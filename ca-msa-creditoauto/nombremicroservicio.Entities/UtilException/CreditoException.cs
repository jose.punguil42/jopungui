﻿using nombremicroservicio.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace nombremicroservicio.Entities.UtilException
{
    public class CreditoException: Exception
    {
        public CreditoException(Respuesta mensaje): base()
        {
            mensaje.Ejecucion = false;
        }
    }
}
