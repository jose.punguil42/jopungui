﻿using System;
using System.Collections.Generic;
using System.Text;

namespace nombremicroservicio.Entities.Models
{
    public class Respuesta
    {
        public bool Ejecucion { get; set; }
        public int Codigo { get; set; }
        public string Mensaje { get; set; } = string.Empty;
        public object Objeto { get; set; }
        public bool Verificacion { get; set; }
    }
}
