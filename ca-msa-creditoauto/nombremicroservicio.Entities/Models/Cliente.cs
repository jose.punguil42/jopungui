﻿using System;
using System.Collections.Generic;

#nullable disable

namespace nombremicroservicio.Entities.Models
{
    public partial class Cliente
    {
        public Cliente()
        {
            SolicitudCreditos = new HashSet<SolicitudCredito>();
        }

        public int ClId { get; set; }
        public string ClIdentificacion { get; set; }
        public string ClNombres { get; set; }
        public int ClEdad { get; set; }
        public DateTime ClFechaNacimiento { get; set; }
        public string ClApellidos { get; set; }
        public string ClDireccion { get; set; }
        public string ClTelefono { get; set; }
        public int ClEstadoCivil { get; set; }
        public string ClIdentificacionConyugue { get; set; }
        public string ClNombreConyugue { get; set; }
        public string ClSujetoCredito { get; set; }
        public string ClEstado { get; set; }

        public virtual ICollection<SolicitudCredito> SolicitudCreditos { get; set; }
    }
}
