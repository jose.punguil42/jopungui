﻿using System;
using System.Collections.Generic;

#nullable disable

namespace nombremicroservicio.Entities.Models
{
    public partial class Vehiculo  //: IBaseController
    {
        public Vehiculo()
        {
            SolicitudCreditos = new HashSet<SolicitudCredito>();
        }

        public int VeId { get; set; }
        public string VePlaca { get; set; }
        public string VeModelo { get; set; }
        public string VeChasis { get; set; }
        public int MaId { get; set; }
        public int? VeTipo { get; set; }
        public string VeCilindraje { get; set; }
        public decimal VeAvaluo { get; set; }

        public virtual Marca Ma { get; set; }
        public virtual ICollection<SolicitudCredito> SolicitudCreditos { get; set; }
    }
}
