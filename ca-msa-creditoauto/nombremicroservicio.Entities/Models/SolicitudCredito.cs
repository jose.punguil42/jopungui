﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace nombremicroservicio.Entities.Models
{
    public partial class SolicitudCredito
    {
        public int ScId { get; set; }

        [Required]
        public DateTime? ScFechaElaboracion { get; set; }

        [Required]
        public int ClId { get; set; }

        [Required]
        public int? PaId { get; set; }

        [Required]
        public int VeId { get; set; }

        [Required]
        public int ScMesesPlazo { get; set; }

        [Required]
        public int ScCuotas { get; set; }

        [Required]
        public decimal ScEntrada { get; set; }

        [Required]
        public int EjId { get; set; }
        public string ScObservacion { get; set; }
        public int? ScEstado { get; set; }
        public DateTime? ClFechaAsginacion { get; set; }
        public bool? ClAsignacion { get; set; }
        public virtual Cliente Cl { get; set; }
        public virtual Ejecutivo Ej { get; set; }
        public virtual Vehiculo Ve { get; set; }
    }
}
