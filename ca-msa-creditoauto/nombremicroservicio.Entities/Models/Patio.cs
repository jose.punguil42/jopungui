﻿using System;
using System.Collections.Generic;

#nullable disable

namespace nombremicroservicio.Entities.Models
{
    public partial class Patio
    {
        public Patio()
        {
            Ejecutivos = new HashSet<Ejecutivo>();
        }

        public int PaId { get; set; }
        public string PaNombre { get; set; }
        public string PaDireccion { get; set; }
        public string PaTelefono { get; set; }
        public int PaNumeroVenta { get; set; }
        public string PaEstado { get; set; }

        public virtual ICollection<Ejecutivo> Ejecutivos { get; set; }
    }
}
