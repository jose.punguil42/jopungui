﻿using System;
using System.Collections.Generic;

#nullable disable

namespace nombremicroservicio.Entities.Models
{
    public partial class Ejecutivo
    {
        public Ejecutivo()
        {
            SolicitudCreditos = new HashSet<SolicitudCredito>();
        }

        public int EjId { get; set; }
        public string EjIdentificacion { get; set; }
        public string EjNombre { get; set; }
        public string EjApellido { get; set; }
        public string EjDireccion { get; set; }
        public string EjTelefono { get; set; }
        public string EjCelular { get; set; }
        public int EjEdad { get; set; }
        public string EjEstado { get; set; }
        public int PaId { get; set; }

        public virtual Patio Pa { get; set; }
        public virtual ICollection<SolicitudCredito> SolicitudCreditos { get; set; }
    }
}
