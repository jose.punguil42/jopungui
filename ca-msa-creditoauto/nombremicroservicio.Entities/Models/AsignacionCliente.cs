﻿using System;
using System.Collections.Generic;
using System.Text;

namespace nombremicroservicio.Entities.Models
{
    public class AsignacionCliente
    {
        public int IdCliente { get; set; }
        public DateTime FechaAsignacion { get; set; }
        public int IdPatio { get; set; }
    }
}
