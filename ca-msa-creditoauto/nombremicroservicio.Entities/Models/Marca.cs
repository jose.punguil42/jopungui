﻿using System;
using System.Collections.Generic;

#nullable disable

namespace nombremicroservicio.Entities.Models
{
    public partial class Marca
    {
        public Marca()
        {
            Vehiculos = new HashSet<Vehiculo>();
        }

        public int MaId { get; set; }
        public string MaNombre { get; set; }
        public string MaEstado { get; set; }

        public virtual ICollection<Vehiculo> Vehiculos { get; set; }
    }
}
