﻿using Microsoft.EntityFrameworkCore;
using nombremicroservicio.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace nombremicroservicio.Test
{
    public class BaseTest
    {
        protected PichinchaContext CrearContext(string baseDatos)
        {
            var dbBase = new DbContextOptionsBuilder<PichinchaContext>().UseInMemoryDatabase(baseDatos).Options;
            var dbContext = new PichinchaContext(dbBase);
            return dbContext;
        }
    }
}
