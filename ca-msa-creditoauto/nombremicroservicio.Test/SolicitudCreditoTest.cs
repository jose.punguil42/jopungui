﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using nombremicroservicio.Domain.Interfaces;
using nombremicroservicio.Entities.Models;
using nombremicroservicio.Infrastructure.Services;
using System;
using System.Threading.Tasks;

namespace nombremicroservicio.Test
{
    [TestClass]
    public class SolicitudCreditoTest: BaseTest
    {
        [TestMethod]
        public async Task ValidarSiUnClienteTieneSolicitudRegistradaEnElDia()
        {
            //Preparación 
            string baseDatos = Guid.NewGuid().ToString();
            var context = CrearContext(baseDatos);
            Respuesta respuesta = new()
            {
                Verificacion = true
            };

            #region Preparar datos clientes
            Cliente cliente = new Cliente
            {
                ClId = 3,
                ClIdentificacion = "1720477348",
                ClNombres = "Mayra Gabriela",
                ClEdad = 35,
                ClFechaNacimiento = DateTime.Now,
                ClApellidos = "Altamirano Carrera",
                ClDireccion = "Casa direcci",
                ClTelefono = "0995691214",
                ClEstadoCivil = 1,
                ClIdentificacionConyugue = "1720477346",
                ClNombreConyugue = "Jose Punguil",
                ClSujetoCredito = "A"
            };
            context.Add(cliente);
            await context.SaveChangesAsync();
            #endregion

            #region Preparar datos Solicitudes creditos
            SolicitudCredito solicitud = new SolicitudCredito
            {
                ScId = 13,
                ScFechaElaboracion = DateTime.Now,
                ClId = 3,
                PaId = 0,
                VeId = 3,
                ScMesesPlazo = 10,
                ScCuotas = 10,
                ScEntrada = 4000,
                EjId = 1,
                ScObservacion = "Solicitud tres",
                ScEstado = 1,
                ClFechaAsginacion = DateTime.Now,
                ClAsignacion = false
            };
            context.Add(solicitud);
            await context.SaveChangesAsync();
            #endregion

            //Prueba
            ISolicitudCredito _servicio = new SrvSolicitudCredito(context);
            await _servicio.ValidarSolicitudClientePorDia(solicitud, respuesta);

            //verificacion 
            Assert.AreEqual(respuesta.Codigo, 333);
        }

        [TestMethod]
        public async Task ValidarSiUnClienteNoTieneSolicitudRegistradaEnElDia()
        {
            //Preparación 
            string baseDatos = Guid.NewGuid().ToString();
            var context = CrearContext(baseDatos);
            Respuesta respuesta = new()
            {
                Verificacion = true,
                Codigo = 100
            };

            #region Preparar datos clientes
            Cliente cliente = new()
            {
                ClId = 1,
                ClIdentificacion = "1720477348",
                ClNombres = "Mayra Gabriela",
                ClEdad = 35,
                ClFechaNacimiento = DateTime.Now,
                ClApellidos = "Altamirano Carrera",
                ClDireccion = "Casa direcci",
                ClTelefono = "0995691214",
                ClEstadoCivil = 1,
                ClIdentificacionConyugue = "1720477346",
                ClNombreConyugue = "Jose Punguil",
                ClSujetoCredito = "A"
            };
            context.Add(cliente);
            await context.SaveChangesAsync();
            #endregion

            #region Preparar datos Solicitudes creditos
            SolicitudCredito solicitudBase = new SolicitudCredito
            {
                ScId = 13,
                ScFechaElaboracion = DateTime.Now.AddDays(2),
                ClId = 1,
                PaId = 0,
                VeId = 3,
                ScMesesPlazo = 10,
                ScCuotas = 10,
                ScEntrada = 4000,
                EjId = 1,
                ScObservacion = "Solicitud tres",
                ScEstado = 1,
                ClFechaAsginacion = DateTime.Now.AddDays(2),
                ClAsignacion = false
            };

            SolicitudCredito solicitudPeticion = new SolicitudCredito
            {
                ScId = 13,
                ScFechaElaboracion = DateTime.Now,
                ClId = 1,
                PaId = 0,
                VeId = 3,
                ScMesesPlazo = 10,
                ScCuotas = 10,
                ScEntrada = 4000,
                EjId = 1,
                ScObservacion = "Solicitud tres",
                ScEstado = 1,
                ClFechaAsginacion = DateTime.Now,
                ClAsignacion = false
            };
            context.Add(solicitudBase);
            await context.SaveChangesAsync();
            #endregion

            //Prueba
            ISolicitudCredito _servicio = new SrvSolicitudCredito(context);
            await _servicio.ValidarSolicitudClientePorDia(solicitudPeticion, respuesta);

            //verificacion 
            Assert.AreEqual(respuesta.Codigo, 100);
        }

        [TestMethod]
        public async Task ValidarSiVehiculoEstaReservado()
        {
            //Preparación 
            string baseDatos = Guid.NewGuid().ToString();
            var context = CrearContext(baseDatos);
            Respuesta respuesta = new()
            {
                Verificacion = true,
                Codigo = 100
            };

            #region Preparar datos Solicitudes creditos
            SolicitudCredito solicitudBase = new SolicitudCredito
            {
                ScId = 13,
                ScFechaElaboracion = DateTime.Now.AddDays(2),
                ClId = 1,
                PaId = 0,
                VeId = 3,
                ScMesesPlazo = 10,
                ScCuotas = 10,
                ScEntrada = 4000,
                EjId = 1,
                ScObservacion = "Solicitud tres",
                ScEstado = 1,
                ClFechaAsginacion = DateTime.Now.AddDays(2),
                ClAsignacion = false
            };
            context.Add(solicitudBase);
            await context.SaveChangesAsync();
            #endregion

            #region Preparar datos vehiculo

            Vehiculo vehiculo = new()
            {
                VeId = 3,
                VePlaca = "123ABC",
                VeModelo = "Modelo C modificado",
                VeChasis = "12345C",
                MaId = 1,
                VeTipo = 1,
                VeCilindraje = "1.6",
                VeAvaluo = 20000
            };
            context.Add(vehiculo);
            await context.SaveChangesAsync();
            #endregion

            //Prueba
            ISolicitudCredito _servicio = new SrvSolicitudCredito(context);
            await _servicio.ValidarVehiculoReservado(solicitudBase, respuesta);

            //verificacion 
            Assert.AreEqual(respuesta.Codigo, 334);
        }

        [TestMethod]
        public async Task ValidarSiVehiculoNoEstaReservado()
        {
            //Preparación 
            string baseDatos = Guid.NewGuid().ToString();
            var context = CrearContext(baseDatos);
            Respuesta respuesta = new()
            {
                Verificacion = true,
                Codigo = 100
            };

            #region Preparar datos Solicitudes creditos
            SolicitudCredito solicitudBase = new SolicitudCredito
            {
                ScId = 13,
                ScFechaElaboracion = DateTime.Now.AddDays(2),
                ClId = 1,
                PaId = 0,
                VeId = 3,
                ScMesesPlazo = 10,
                ScCuotas = 10,
                ScEntrada = 4000,
                EjId = 1,
                ScObservacion = "Solicitud tres",
                ScEstado = 3,
                ClFechaAsginacion = DateTime.Now.AddDays(2),
                ClAsignacion = false
            };
            context.Add(solicitudBase);
            await context.SaveChangesAsync();
            #endregion

            #region Preparar datos vehiculo

            Vehiculo vehiculo = new()
            {
                VeId = 3,
                VePlaca = "123ABC",
                VeModelo = "Modelo C modificado",
                VeChasis = "12345C",
                MaId = 1,
                VeTipo = 1,
                VeCilindraje = "1.6",
                VeAvaluo = 20000
            };
            context.Add(vehiculo);
            await context.SaveChangesAsync();
            #endregion

            //Prueba
            ISolicitudCredito _servicio = new SrvSolicitudCredito(context);
            await _servicio.ValidarVehiculoReservado(solicitudBase, respuesta);

            //verificacion 
            Assert.AreEqual(respuesta.Codigo, 100);
        }
    }
}
