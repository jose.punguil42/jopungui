﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using nombremicroservicio.Entities.Models;

#nullable disable

namespace nombremicroservicio.Repository
{
    public partial class PichinchaContext : DbContext
    {
        public PichinchaContext()
        {
        }

        public PichinchaContext(DbContextOptions<PichinchaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Cliente> Clientes { get; set; }
        public virtual DbSet<Ejecutivo> Ejecutivos { get; set; }
        public virtual DbSet<Marca> Marcas { get; set; }
        public virtual DbSet<Patio> Patios { get; set; }
        public virtual DbSet<SolicitudCredito> SolicitudCreditos { get; set; }
        public virtual DbSet<Vehiculo> Vehiculos { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Cliente>(entity =>
            {
                entity.HasKey(e => e.ClId);

                entity.ToTable("Cliente");

                entity.Property(e => e.ClId).HasColumnName("cl_id");

                entity.Property(e => e.ClApellidos)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("cl_apellidos");

                entity.Property(e => e.ClDireccion)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("cl_direccion");

                entity.Property(e => e.ClEdad).HasColumnName("cl_edad");

                entity.Property(e => e.ClEstado)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("cl_estado")
                    .IsFixedLength(true);

                entity.Property(e => e.ClEstadoCivil).HasColumnName("cl_estado_civil");

                entity.Property(e => e.ClFechaNacimiento)
                    .HasColumnType("datetime")
                    .HasColumnName("cl_fecha_nacimiento");

                entity.Property(e => e.ClIdentificacion)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("cl_identificacion");

                entity.Property(e => e.ClIdentificacionConyugue)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("cl_identificacion_conyugue");

                entity.Property(e => e.ClNombreConyugue)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("cl_nombre_conyugue");

                entity.Property(e => e.ClNombres)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("cl_nombres");

                entity.Property(e => e.ClSujetoCredito)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("cl_sujeto_credito")
                    .IsFixedLength(true);

                entity.Property(e => e.ClTelefono)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("cl_telefono");
            });

            modelBuilder.Entity<Ejecutivo>(entity =>
            {
                entity.HasKey(e => e.EjId);

                entity.ToTable("Ejecutivo");

                entity.Property(e => e.EjId).HasColumnName("ej_id");

                entity.Property(e => e.EjApellido)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ej_apellido");

                entity.Property(e => e.EjCelular)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasColumnName("ej_celular");

                entity.Property(e => e.EjDireccion)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ej_direccion");

                entity.Property(e => e.EjEdad).HasColumnName("ej_edad");

                entity.Property(e => e.EjEstado)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("ej_estado")
                    .IsFixedLength(true);

                entity.Property(e => e.EjIdentificacion)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("ej_identificacion");

                entity.Property(e => e.EjNombre)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ej_nombre");

                entity.Property(e => e.EjTelefono)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasColumnName("ej_telefono");

                entity.Property(e => e.PaId).HasColumnName("pa_id");

                entity.HasOne(d => d.Pa)
                    .WithMany(p => p.Ejecutivos)
                    .HasForeignKey(d => d.PaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Ejecutivo_Patio");
            });

            modelBuilder.Entity<Marca>(entity =>
            {
                entity.HasKey(e => e.MaId);

                entity.ToTable("Marca");

                entity.Property(e => e.MaId).HasColumnName("ma_id");

                entity.Property(e => e.MaEstado)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("ma_estado")
                    .IsFixedLength(true);

                entity.Property(e => e.MaNombre)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ma_nombre");
            });

            modelBuilder.Entity<Patio>(entity =>
            {
                entity.HasKey(e => e.PaId);

                entity.ToTable("Patio");

                entity.Property(e => e.PaId).HasColumnName("pa_id");

                entity.Property(e => e.PaDireccion)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("pa_direccion");

                entity.Property(e => e.PaEstado)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("pa_estado")
                    .IsFixedLength(true);

                entity.Property(e => e.PaNombre)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("pa_nombre");

                entity.Property(e => e.PaNumeroVenta).HasColumnName("pa_numero_venta");

                entity.Property(e => e.PaTelefono)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("pa_telefono");
            });

            modelBuilder.Entity<SolicitudCredito>(entity =>
            {
                entity.HasKey(e => e.ScId);

                entity.ToTable("Solicitud_Credito");

                entity.Property(e => e.ScId).HasColumnName("sc_id");

                entity.Property(e => e.ClAsignacion).HasColumnName("cl_asignacion");

                entity.Property(e => e.ClFechaAsginacion)
                    .HasColumnType("datetime")
                    .HasColumnName("cl_fecha_asginacion");

                entity.Property(e => e.ClId).HasColumnName("cl_id");

                entity.Property(e => e.EjId).HasColumnName("ej_id");

                entity.Property(e => e.PaId).HasColumnName("pa_id");

                entity.Property(e => e.ScCuotas).HasColumnName("sc_cuotas");

                entity.Property(e => e.ScEntrada)
                    .HasColumnType("money")
                    .HasColumnName("sc_entrada");

                entity.Property(e => e.ScEstado).HasColumnName("sc_estado");

                entity.Property(e => e.ScFechaElaboracion)
                    .HasColumnType("datetime")
                    .HasColumnName("sc_fecha_elaboracion");

                entity.Property(e => e.ScMesesPlazo).HasColumnName("sc_meses_plazo");

                entity.Property(e => e.ScObservacion)
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasColumnName("sc_observacion");

                entity.Property(e => e.VeId).HasColumnName("ve_id");

                entity.HasOne(d => d.Cl)
                    .WithMany(p => p.SolicitudCreditos)
                    .HasForeignKey(d => d.ClId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Solicitud_Credito_Cliente");

                entity.HasOne(d => d.Ej)
                    .WithMany(p => p.SolicitudCreditos)
                    .HasForeignKey(d => d.EjId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Solicitud_Credito_Ejecutivo");

                entity.HasOne(d => d.Ve)
                    .WithMany(p => p.SolicitudCreditos)
                    .HasForeignKey(d => d.VeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Solicitud_Credito_Vehiculo");
            });

            modelBuilder.Entity<Vehiculo>(entity =>
            {
                entity.HasKey(e => e.VeId);

                entity.ToTable("Vehiculo");

                entity.Property(e => e.VeId).HasColumnName("ve_id");

                entity.Property(e => e.MaId).HasColumnName("ma_id");

                entity.Property(e => e.VeAvaluo)
                    .HasColumnType("money")
                    .HasColumnName("ve_avaluo");

                entity.Property(e => e.VeChasis)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ve_chasis");

                entity.Property(e => e.VeCilindraje)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ve_cilindraje");

                entity.Property(e => e.VeModelo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ve_modelo");

                entity.Property(e => e.VePlaca)
                    .IsRequired()
                    .HasMaxLength(7)
                    .IsUnicode(false)
                    .HasColumnName("ve_placa");

                entity.Property(e => e.VeTipo).HasColumnName("ve_tipo");

                entity.HasOne(d => d.Ma)
                    .WithMany(p => p.Vehiculos)
                    .HasForeignKey(d => d.MaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Vehiculo_Marca");
            });
        }
    }
}
