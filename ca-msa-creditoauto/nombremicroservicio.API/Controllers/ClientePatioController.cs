﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using nombremicroservicio.Domain.Interfaces;
using nombremicroservicio.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace nombremicroservicio.API.Controllers
{
    [Route("api")]
    [ApiController]
    public class ClientePatioController : ControllerBase
    {
        private readonly IAsignacionCliente servicio;
        public ClientePatioController(IAsignacionCliente _servicio)
        {
            servicio = _servicio;
        }
        /// <summary>
        /// Se edita el patio del cliente que esta en la solicitud de credito 
        /// </summary>
        /// <param name="oSignacionCliente"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("ClientePatio")]
        public async Task<Respuesta> EditarAsignacion(AsignacionCliente oSignacionCliente)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                if (oSignacionCliente.IdCliente == 0)
                {
                    respuesta.Mensaje = $"El cliente no existe";
                }
                else
                {
                    respuesta = await servicio.EditarAsignacion(oSignacionCliente);
                    respuesta.Ejecucion = true;
                }
            }
            catch (Exception e)
            {
                respuesta.Mensaje = "Ocurrio un error: " + e.StackTrace;
            }
            return respuesta;

        }

        /// <summary>
        /// Quita la asignacion del Patio, asignacion y fecha de asignacion 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("ClientePatio/{idCliente}")]
        public async Task<Respuesta> EliminarAsignacion(int idCliente)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                respuesta = await servicio.EliminarAsignacion(idCliente);
            }
            catch (Exception e)
            {
                respuesta.Mensaje = "Ocurrio un error: " + e.StackTrace;
            }
            return respuesta;
        }
    }
}
