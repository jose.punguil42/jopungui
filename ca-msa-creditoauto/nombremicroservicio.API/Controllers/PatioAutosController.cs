﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using nombremicroservicio.Domain.Interfaces;
using nombremicroservicio.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static nombremicroservicio.Entities.Utilities.Enumeradores;

namespace nombremicroservicio.API.Controllers
{
    [Route("api")]
    [ApiController]
    public class PatioAutosController : ControllerBase
    {
        private readonly IPatioAuto servicio;

        public PatioAutosController(IPatioAuto _servicio)
        {
            servicio = _servicio;
        }

        [HttpGet]
        [Route("PatioAutos")]
        public async Task<Respuesta> PatioAutos()
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                respuesta = await servicio.ConsultarPatioAutos();
            }
            catch (Exception e)
            {
                respuesta.Mensaje = "Ocurrio un error: " + e.StackTrace;
                respuesta.Codigo = (int)CodigoError.ErrorGenerico;
            }
            return respuesta;
        }

        [HttpGet]
        [Route("PatioAutos/{id}")]
        public async Task<Respuesta> PatioAuto(int id)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                respuesta = await servicio.ConsultarPatioAuto(id);
            }
            catch (Exception e)
            {
                respuesta.Mensaje = "Ocurrio un error: " + e.StackTrace;
                respuesta.Codigo = (int)CodigoError.ErrorGenerico;
            }
            return respuesta;
        }

        [HttpPost]
        [Route("PatioAuto")]
        public async Task<Respuesta> CrearPatioAuto(Patio oPatioAuto)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                respuesta = await servicio.CrearPatioAuto(oPatioAuto);
            }
            catch (Exception e)
            {
                respuesta.Mensaje = "Ocurrio un error: " + e.StackTrace;
                respuesta.Codigo = (int)CodigoError.ErrorGenerico;
            }
            return respuesta;
        }

        [HttpPut]
        [Route("PatioAutos/{id}")]
        public async Task<Respuesta> EditarPatioAuto(int id, Patio oPatioAuto)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                if (id != oPatioAuto.PaNumeroVenta)
                {
                    respuesta.Mensaje = $"El id no coincide: {id}";
                    respuesta.Codigo = (int)CodigoError.ErrorGenerico;
                }
                else
                {
                    respuesta = await servicio.EditarPatioAuto(oPatioAuto);
                }
            }
            catch (Exception e)
            {
                respuesta.Mensaje = "Ocurrio un error: " + e.StackTrace;
                respuesta.Codigo = (int)CodigoError.ErrorGenerico;
            }
            return respuesta;
        }

        [HttpDelete]
        [Route("PatioAuto/{id}")]
        public async Task<Respuesta> EliminarPatioAuto(int id)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                respuesta = await servicio.EliminarPatioAuto(id);
            }
            catch (Exception e)
            {
                respuesta.Mensaje = "Ocurrio un error: " + e.StackTrace;
                respuesta.Codigo = (int)CodigoError.ErrorGenerico;
            }
            return respuesta;
        }
    }
}
