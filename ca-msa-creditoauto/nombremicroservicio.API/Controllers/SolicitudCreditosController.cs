﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using nombremicroservicio.Domain.Interfaces;
using nombremicroservicio.Entities.Models;
using nombremicroservicio.Entities.UtilException;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static nombremicroservicio.Entities.Utilities.Enumeradores;

namespace nombremicroservicio.API.Controllers
{
    [Route("api")]
    [ApiController]
    public class SolicitudCreditosController : ControllerBase
    {
        private readonly ISolicitudCredito servicio;

        public SolicitudCreditosController(ISolicitudCredito _solicitudCredito)
        {
            servicio = _solicitudCredito;
        }

        [HttpGet]
        [Route("Solicitudes")]
        public async Task<Respuesta> Solicitudes()
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                respuesta = await servicio.ConsultarSolicitudes();
            }
            catch (Exception e)
            {
                respuesta.Mensaje = "Ocurrio un error: " + e.StackTrace;
                respuesta.Codigo = (int)CodigoError.ErrorGenerico;
            }
            return respuesta;
        }

        [HttpPost]
        [Route("solicitud")]
        public async Task<Respuesta> SolicitudCredito(SolicitudCredito solicitudCredito)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                respuesta = await servicio.CrearSolicitudCredito(solicitudCredito, respuesta);
            }
            catch (CreditoException)
            {
                return respuesta;
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                return respuesta;
            }
            return respuesta;
        }
    }
}
