﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using nombremicroservicio.Domain.Interfaces;
using nombremicroservicio.Entities.Models;
using nombremicroservicio.Repository;
using static nombremicroservicio.Entities.Utilities.Enumeradores;

namespace nombremicroservicio.API.Controllers
{
    [Route("api")]
    [ApiController]
    public class ClientesController : ControllerBase
    {
        private readonly ICliente servicio;

        public ClientesController(ICliente _servicio)
        {
            servicio = _servicio;
        }

        [HttpGet]
        [Route("CargarCliente")]
        public async Task<Respuesta> CargarClientes()
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                respuesta = await servicio.CargarClientes();
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Ocurrio un error: " + ex.StackTrace;
            }
            return respuesta;
        }

        [HttpGet]
        [Route("Cliente/{id}")]
        public async Task<Respuesta> ConsultarCliente(int id)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                respuesta = await servicio.ConsultarCliente(id);
            }
            catch (Exception e)
            {
                respuesta.Mensaje = "Ocurrio un error: " + e.StackTrace;
                respuesta.Codigo = (int)CodigoError.ErrorGenerico;
            }
            return respuesta;
        }

        [HttpGet]
        [Route("Clientes")]
        public async Task<Respuesta> Clientes()
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                respuesta = await servicio.ConsultarClientes();
            }
            catch (Exception e)
            {
                respuesta.Mensaje = "Ocurrio un error: " + e.StackTrace;
                respuesta.Codigo = (int)CodigoError.ErrorGenerico;
            }
            return respuesta;
        }

        [HttpPost]
        [Route("Cliente")]
        public async Task<Respuesta> Cliente(Cliente oCLiente)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                respuesta = await servicio.CrearCliente(oCLiente);
            }
            catch (Exception e)
            {
                respuesta.Mensaje = "Error: " + e.StackTrace;
                respuesta.Codigo = (int)CodigoError.ErrorGenerico;
            }
            return respuesta;
        }

        [HttpPut]
        [Route("Cliente/{id}")]
        public async Task<Respuesta> EditarCliente(int id, Cliente oCliente)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                if (id != oCliente.ClId)
                {
                    respuesta.Mensaje = $"Error en el id: {id}";
                    respuesta.Codigo = (int)CodigoError.ErrorGenerico;
                }
                else
                {
                    respuesta = await servicio.EditarCliente(oCliente);
                }
            }
            catch (Exception e)
            {
                respuesta.Mensaje = "Error: " + e.StackTrace;
                respuesta.Codigo = (int)CodigoError.ErrorGenerico;
            }
            return respuesta;
        }

        [HttpDelete]
        [Route("Cliente/{id}")]
        public async Task<Respuesta> EliminarCliente(int id)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                respuesta = await servicio.EliminarCliente(id);
            }
            catch (Exception e)
            {
                respuesta.Mensaje = "Error: " + e.StackTrace;
                respuesta.Codigo = (int)CodigoError.ErrorGenerico;
            }
            return respuesta;
        }
    }
}
