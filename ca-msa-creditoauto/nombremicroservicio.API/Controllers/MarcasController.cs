﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using nombremicroservicio.Domain.Interfaces;
using nombremicroservicio.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static nombremicroservicio.Entities.Utilities.Enumeradores;

namespace nombremicroservicio.API.Controllers
{
    [Route("api")]
    [ApiController]
    public class MarcasController : ControllerBase
    {
        private readonly IMarca servicio;

        public MarcasController(IMarca _servicio)
        {
            servicio = _servicio;
        }

        [HttpGet]
        [Route("CargarMarcas")]
        public async Task<Respuesta> CargarMarcas()
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                respuesta = await servicio.CargarMarcas();
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Ocurrio un error: " + ex.StackTrace;
            }
            return respuesta;
        }

        [HttpGet]
        [Route("Marca/{id}")]
        public async Task<Respuesta> ConsultarMarca(int id)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                respuesta = await servicio.GetMarca(id);
            }
            catch (Exception e)
            {
                respuesta.Mensaje = "Ocurrio un error: " + e.StackTrace;
                respuesta.Codigo = (int)CodigoError.ErrorGenerico;
            }
            return respuesta;
        }
    }
}
