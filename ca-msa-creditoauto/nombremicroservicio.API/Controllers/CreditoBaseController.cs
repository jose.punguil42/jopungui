﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using nombremicroservicio.Domain.Interfaces;
using nombremicroservicio.Entities.Models;
using nombremicroservicio.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace nombremicroservicio.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CreditoBaseController<TEntity> : ControllerBase
        where TEntity : class
    {
        private readonly PichinchaContext _context;
        private readonly string _nombreControlador;

        public CreditoBaseController(PichinchaContext context, string nombreControlador)
        {
            _context = context;
            _nombreControlador = nombreControlador;
        }

        [HttpGet]
        public virtual async Task<List<TEntity>> Get()
        {
            try
            {
                return await _context.Set<TEntity>().ToListAsync();
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        //[HttpGet("{id}")]
        //public virtual async Task<ActionResult<TEntity>>Get(int id)
        //{
        //    //var entity = await _context.Set<TEntity>().FirstOrDefaultAsync(c => c.Id == id);
        //    //if (entity == null)
        //    //{
        //    //    return NotFound();
        //    //}
        //    //return entity;
        //}
    }
}
