﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using nombremicroservicio.Domain.Interfaces;
using nombremicroservicio.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static nombremicroservicio.Entities.Utilities.Enumeradores;

namespace nombremicroservicio.API.Controllers
{
    [Route("api")]
    [ApiController]
    public class EjecutivosController : ControllerBase
    {
        private readonly IEjecutivo servicio;

        public EjecutivosController(IEjecutivo _servicio)
        {
            servicio = _servicio;
        }

        [HttpGet]
        [Route("CargarEjecutivo")]
        public async Task<Respuesta> CargarEjecutivos()
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                respuesta = await servicio.CargarEjecutivos();
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Ocurrio un error: " + ex.StackTrace;
            }
            return respuesta;
        }

        [HttpGet]
        [Route("Ejecutivo/{id}")]
        public async Task<Respuesta> GetEjecutivo(int id)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                respuesta = await servicio.GetEjecutivo(id);
            }
            catch (Exception e)
            {
                respuesta.Mensaje = "Ocurrio un error: " + e.StackTrace;
                respuesta.Codigo = (int)CodigoError.ErrorGenerico;
            }
            return respuesta;
        }

        [HttpGet("Patio/{identificacionPatio}/ejecutivos")]
        public async Task<Respuesta> ObtenerEjecutivoPatio(int identificacionPatio)
        {
            return await servicio.ObtenerEjecutivoPatio(identificacionPatio);
        }
    }
}
