﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using nombremicroservicio.Domain.Interfaces;
using nombremicroservicio.Entities.Models;
using nombremicroservicio.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static nombremicroservicio.Entities.Utilities.Enumeradores;

namespace nombremicroservicio.API.Controllers
{
    [Route("api")]
    [ApiController]
    public class VehiculosController
    {
        private readonly IVehiculo servicio;

        public VehiculosController(IVehiculo _servicio)
        {
            servicio = _servicio;
        }

        [HttpGet]
        [Route("Vehiculos")]
        public async Task<Respuesta> Vehiculos()
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                respuesta = await servicio.ConsultarVehiculos();
            }
            catch (Exception e)
            {
                respuesta.Mensaje = "Ocurrio un error: " + e.StackTrace;
                respuesta.Codigo = (int)CodigoError.ErrorGenerico;
            }
            return respuesta;
        }

        [HttpGet]
        [Route("Vehiculo/{placa}")]
        public async Task<Respuesta> Vehiculo(string placa)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                respuesta = await servicio.ConsultarVehiculo(placa);
            }
            catch (Exception e)
            {
                respuesta.Mensaje = "Ocurrio un error: " + e.StackTrace;
                respuesta.Codigo = (int)CodigoError.ErrorGenerico;
            }
            return respuesta;
        }

        [HttpPost]
        [Route("Vehiculo")]
        public async Task<Respuesta> CrearVehiculo(Vehiculo oVehiculo)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                respuesta = await servicio.CrearVehiculo(oVehiculo);
            }
            catch (Exception e)
            {
                respuesta.Mensaje = "Ocurrio un error: " + e.StackTrace;
                respuesta.Codigo = (int)CodigoError.ErrorGenerico;
            }
            return respuesta;
        }

        [HttpPut]
        [Route("Vehiculo/{placa}")]
        public async Task<Respuesta> EditarVehiculo(string placa, Vehiculo oVehiculo)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                if (placa != oVehiculo.VePlaca)
                {
                    respuesta.Mensaje = $"La placa no coincide: {placa}";
                    respuesta.Codigo = (int)CodigoError.ErrorGenerico;
                }
                else
                {
                    respuesta.Objeto = await servicio.EditarVehiculo(oVehiculo);
                }
            }
            catch (Exception e)
            {
                respuesta.Mensaje = "Ocurrio un error: " + e.StackTrace;
                respuesta.Codigo = (int)CodigoError.ErrorGenerico;
            }
            return respuesta;
        }

        [HttpDelete]
        [Route("Vehiculo/{placa}")]
        public async Task<Respuesta> EliminarVehiculo(string placa)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                respuesta = await servicio.EliminarVehiculo(placa);
            }
            catch (Exception e)
            {
                respuesta.Mensaje = "Ocurrio un error: " + e.StackTrace;
                respuesta.Codigo = (int)CodigoError.ErrorGenerico;
            }
            return respuesta;

        }
    }
}
