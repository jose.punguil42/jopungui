﻿using nombremicroservicio.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace nombremicroservicio.Domain.Interfaces
{
    public interface IVehiculo
    {
        Task<Respuesta> ConsultarVehiculos();
        Task<Respuesta> ConsultarVehiculo(string placa);
        Task<Respuesta> CrearVehiculo(Vehiculo vehiculo);
        Task<Respuesta> EditarVehiculo(Vehiculo vehiculo);
        Task<Respuesta> EliminarVehiculo(string placa);
    }
}
