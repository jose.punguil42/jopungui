﻿using nombremicroservicio.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace nombremicroservicio.Domain.Interfaces
{
    public interface ISolicitudCredito
    {
        Task<Respuesta> ConsultarSolicitudes();
        Task<Respuesta> CrearSolicitudCredito(SolicitudCredito solicitud, Respuesta respuesta);
        Task ValidarSolicitudClientePorDia(SolicitudCredito solicitud, Respuesta respuesta);
        Task ValidarVehiculoReservado(SolicitudCredito solicitud, Respuesta respuesta);
        Task SolicitudCredito(SolicitudCredito solicitud, Respuesta respuesta);
    }
}
