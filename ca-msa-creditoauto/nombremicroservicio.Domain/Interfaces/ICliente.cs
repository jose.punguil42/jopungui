﻿using nombremicroservicio.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace nombremicroservicio.Domain.Interfaces
{
    public interface ICliente
    {
        Task<Respuesta> CargarClientes();
        Task<Respuesta> ConsultarCliente(int id);
        Task<Respuesta> CrearCliente(Cliente oCliente);
        Task<Respuesta> ConsultarClientes();
        Task<Respuesta> EditarCliente(Cliente oCliente);
        Task<Respuesta> EliminarCliente(int id);
    }
}
