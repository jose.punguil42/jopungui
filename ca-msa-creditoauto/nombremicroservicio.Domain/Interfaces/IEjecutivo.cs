﻿using nombremicroservicio.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace nombremicroservicio.Domain.Interfaces
{
    public interface IEjecutivo
    {
        Task<Respuesta> CargarEjecutivos();
        Task<Respuesta> GetEjecutivo(int id);
        Task<Respuesta> ObtenerEjecutivoPatio(int identificacionPatio);
    }
}
