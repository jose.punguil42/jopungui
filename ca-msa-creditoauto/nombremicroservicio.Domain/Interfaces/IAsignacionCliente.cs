﻿using nombremicroservicio.Entities.Models;
using System.Threading.Tasks;

namespace nombremicroservicio.Domain.Interfaces
{
    public interface IAsignacionCliente
    {
        Task<Respuesta> EliminarAsignacion(int idCliente);
        Task<Respuesta> EditarAsignacion(AsignacionCliente oSignacionCliente);
    }
}
