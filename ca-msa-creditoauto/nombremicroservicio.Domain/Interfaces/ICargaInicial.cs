﻿using nombremicroservicio.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace nombremicroservicio.Domain.Interfaces
{
    public interface ICargaInicial
    {
        Task<Respuesta> CargarInicial();
    }
}
