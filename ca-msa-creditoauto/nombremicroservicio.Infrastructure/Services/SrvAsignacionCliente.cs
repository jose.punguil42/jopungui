﻿using Microsoft.EntityFrameworkCore;
using nombremicroservicio.Domain.Interfaces;
using nombremicroservicio.Entities.Models;
using nombremicroservicio.Infrastructure.Utilities;
using nombremicroservicio.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace nombremicroservicio.Infrastructure.Services
{
    public class SrvAsignacionCliente: IAsignacionCliente
    {
        private readonly PichinchaContext _context;
        public SrvAsignacionCliente(PichinchaContext context)
        {
            _context = context;
        }
        public async Task<Respuesta> EditarAsignacion(AsignacionCliente oSignacionCliente)
        {
            Respuesta respuesta = new Respuesta();
            var solicitudCredito = await _context.SolicitudCreditos.FirstOrDefaultAsync(x => x.ClId == oSignacionCliente.IdCliente);
            if (solicitudCredito == null)
            {
                respuesta.Mensaje = $"Cliente no tiene asignación";
                return respuesta;
            };
            solicitudCredito.PaId = oSignacionCliente.IdPatio;
            _context.Entry(solicitudCredito).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            respuesta.Objeto = solicitudCredito;
            Validaciones.AsignarRespuestaOk(respuesta);
            return respuesta;
        }
        public async Task<Respuesta> EliminarAsignacion(int idCliente)
        {
            Respuesta respuesta = new Respuesta();
            var solicitudCredito = await _context.SolicitudCreditos.FirstOrDefaultAsync(x => x.ClId == idCliente);
            if (solicitudCredito == null)
            {
                respuesta.Mensaje = $"Cliente no tiene asignación";
                return respuesta;
            }
            solicitudCredito.ClAsignacion = false;
            solicitudCredito.ClFechaAsginacion = null;
            solicitudCredito.PaId = 0;
            _context.Entry(solicitudCredito).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            respuesta.Objeto = solicitudCredito;
            Validaciones.AsignarRespuestaOk(respuesta);
            return respuesta;
        }
    }
}
