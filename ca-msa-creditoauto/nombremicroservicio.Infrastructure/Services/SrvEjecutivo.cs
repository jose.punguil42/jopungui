﻿using Microsoft.EntityFrameworkCore;
using nombremicroservicio.Domain.Interfaces;
using nombremicroservicio.Entities.Models;
using nombremicroservicio.Entities.Utilities;
using nombremicroservicio.Infrastructure.Utilities;
using nombremicroservicio.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nombremicroservicio.Infrastructure.Services
{
    public class SrvEjecutivo: IEjecutivo
    {
        private readonly PichinchaContext _context;
        private string rutaArchivo = "D:\\Banco Pichincha\\Proyectos\\Crédito Automotriz\\Archivos\\Ejecutivos.csv";

        public SrvEjecutivo(PichinchaContext context)
        {
            _context = context;
        }

        public async Task<Respuesta> GetEjecutivo(int id)
        {
            Respuesta respuesta = new Respuesta();
            Ejecutivo ejeutivo = await _context.Ejecutivos.FirstOrDefaultAsync(x => x.EjId == id);
            Validaciones.AsignarRespuestaOk(respuesta, ejeutivo);
            return respuesta;
        }

        public async Task<Respuesta> CargarEjecutivos()
        {
            Respuesta respuesta = new Respuesta();
            int i = 0;
            string regitroError = string.Empty;
            string registroOK = string.Empty;

            StreamReader archivo = new StreamReader(rutaArchivo);
            string separador = ",";
            string linea;
            while ((linea = archivo.ReadLine()) != null)
            {
                i++;
                string[] fila = linea.Split(separador);
                bool tieneError = false;
                if (fila.Length == 8)
                {
                    Ejecutivo oEjecutivo = new Ejecutivo
                    {
                        EjIdentificacion = fila[0],
                        EjNombre = fila[1],
                        EjApellido = fila[2],
                        EjDireccion = fila[3],
                        EjTelefono = fila[4],
                        EjCelular = fila[5],
                        EjEdad = Convert.ToInt32(fila[6]),
                        PaId = Convert.ToInt32(fila[7]),
                    };

                    //Registra ejecutivos en base de datos
                    Respuesta respuestaTemporal = await CrearEjecutivo(oEjecutivo);
                    if (tieneError || !respuestaTemporal.Ejecucion)
                    {
                        regitroError = regitroError + "," + i;
                    }
                    else
                    {
                        registroOK = registroOK + "," + i;
                    }
                }
            }
            respuesta.Mensaje = $"Registros Agregados: {registroOK} -- Registros con error: {regitroError}";
            respuesta.Ejecucion = true;
            return respuesta;
        }

        public async Task<Respuesta> CrearEjecutivo(Ejecutivo oEjecutivo)
        {
            Respuesta respuesta = new Respuesta();
            respuesta = await GetEjecutivo(oEjecutivo.EjId);
            Ejecutivo ejecutivo = (Ejecutivo)respuesta.Objeto;
            if (ejecutivo != null)
            {
                respuesta.Mensaje = $"La identificación ya esta registrada:{oEjecutivo.EjIdentificacion} ";
            }
            else
            {
                _context.Ejecutivos.Add(oEjecutivo);
                respuesta.Mensaje = $"Registrado correctamente:{oEjecutivo.EjIdentificacion} ";
                respuesta.Ejecucion = true;
                await _context.SaveChangesAsync();
            }
            return respuesta;
        }

        public async Task<Respuesta> ObtenerEjecutivoPatio(int identificacionPatio)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                IEnumerable<Ejecutivo> ejecutivos = await _context.Ejecutivos.Where(x => x.PaId == identificacionPatio).ToListAsync();
                respuesta.Objeto = ejecutivos;
                respuesta.Ejecucion = true;
                respuesta.Mensaje = "OK";
                if (ejecutivos == null || ejecutivos.Count() == 0)
                {
                    respuesta.Mensaje = $"No existe ejecutivos en el Patio: { identificacionPatio }";
                }
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.Ejecucion = false;
            }
            return respuesta;
        }
    }
}
