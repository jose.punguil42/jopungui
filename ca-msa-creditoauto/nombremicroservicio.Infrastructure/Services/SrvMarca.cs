﻿using Microsoft.EntityFrameworkCore;
using nombremicroservicio.Domain.Interfaces;
using nombremicroservicio.Entities.Models;
using nombremicroservicio.Infrastructure.Utilities;
using nombremicroservicio.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace nombremicroservicio.Infrastructure.Services
{
    public class SrvMarca : IMarca
    {
        private readonly PichinchaContext _context;
        private string rutaArchivo = "D:\\Banco Pichincha\\Proyectos\\Crédito Automotriz\\Archivos\\Marcas.csv";

        public SrvMarca(PichinchaContext context)
        {
            _context = context;
        }

        public async Task<Respuesta> CargarMarcas()
        {
            Respuesta respuesta = new Respuesta();
            int i = 0;
            string regitroError = string.Empty;
            string registroOK = string.Empty;

            StreamReader archivo = new StreamReader(rutaArchivo);
            string separador = ",";
            string linea;
            while ((linea = archivo.ReadLine()) != null)
            {
                i++;
                string[] fila = linea.Split(separador);
                bool tieneError = false;
                if (fila.Length == 1)
                {
                    Marca oMarca = new Marca
                    {
                        MaNombre = fila[0]
                    };

                    //Registra marca en base de datos
                    Respuesta respuestaTemporal = await CrearMarca(oMarca);
                    if (tieneError || !respuestaTemporal.Ejecucion)
                    {
                        regitroError = regitroError + "," + i;
                    }
                    else
                    {
                        registroOK = registroOK + "," + i;
                    }
                }
            }
            respuesta.Mensaje = $"Registros Agregados: {registroOK} -- Registros con error: {regitroError}";
            respuesta.Ejecucion = true;
            return respuesta;
        }

        public async Task<Respuesta> GetMarca(int id)
        {
            Respuesta respuesta = new Respuesta();
            Marca marca = await _context.Marcas.FirstOrDefaultAsync(x => x.MaId == id);
            Validaciones.AsignarRespuestaOk(respuesta, marca);
            return respuesta;
        }

        public async Task<Marca> ConcultarMarcaId(int id)
        {
            return await _context.Marcas.FirstOrDefaultAsync(x => x.MaId == id);
        }

        public async Task<Respuesta> CrearMarca(Marca oMarca)
        {
            Respuesta respuesta = await GetMarca(oMarca.MaId);
            Marca marca = (Marca)respuesta.Objeto;
            if (marca != null)
            {
                respuesta.Mensaje = $"La marca ya está registrada:{oMarca.MaNombre} ";
            }
            else
            {
                _context.Marcas.Add(oMarca);
                respuesta.Mensaje = $"Registrado correctamente:{oMarca.MaNombre} ";
                respuesta.Ejecucion = true;
                await _context.SaveChangesAsync();
            }
            return respuesta;
        }
    }
}
