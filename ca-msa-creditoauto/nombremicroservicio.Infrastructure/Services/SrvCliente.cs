﻿using Microsoft.EntityFrameworkCore;
using nombremicroservicio.Domain.Interfaces;
using nombremicroservicio.Entities.Models;
using nombremicroservicio.Infrastructure.Utilities;
using nombremicroservicio.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using static nombremicroservicio.Entities.Utilities.Enumeradores;

namespace nombremicroservicio.Infrastructure.Services
{
    public class SrvCliente: ICliente
    {
        private readonly PichinchaContext _context;
        private string rutaArchivo = "D:\\Banco Pichincha\\Proyectos\\Crédito Automotriz\\Archivos\\Clientes.csv";

        public SrvCliente(PichinchaContext context)
        {
            _context = context;
        }

        public async Task<Respuesta> ConsultarCliente(int id)
        {
            Respuesta respuesta = new Respuesta();
            Cliente cliente = await _context.Clientes.FirstOrDefaultAsync(x => x.ClId == id);
            Validaciones.AsignarRespuestaOk(respuesta, cliente);
            return respuesta;
        }

        public async Task<Respuesta> CargarClientes()
        {
            Respuesta respuesta = new Respuesta();
            int i = 0;
            string regitroError = string.Empty;
            string registroOK = string.Empty;

            StreamReader archivo = new StreamReader(rutaArchivo);
            string separador = ",";
            string linea;
            while ((linea = archivo.ReadLine()) != null)
            {
                i++;
                string[] fila = linea.Split(separador);
                bool tieneError = false;
                if (fila.Length == 11)
                {
                    Cliente oCliente = new Cliente
                    {
                        ClIdentificacion = fila[0],
                        ClNombres = fila[1],
                        ClEdad = Convert.ToInt32(fila[2]),
                        ClFechaNacimiento = Convert.ToDateTime(fila[3]),
                        ClApellidos = fila[4],
                        ClDireccion = fila[5],
                        ClTelefono = fila[6],
                        ClEstadoCivil = Convert.ToInt32(fila[7]),
                        ClIdentificacionConyugue = fila[8],
                        ClNombreConyugue = fila[9],
                        ClSujetoCredito = fila[10].Replace(";", "")
                    };

                    //Registra cliente en base de datos
                    Respuesta respuestaTemporal = await CrearCliente(oCliente);
                    if (tieneError || !respuestaTemporal.Ejecucion)
                    {
                        regitroError = regitroError + "," + i;
                    }
                    else
                    {
                        registroOK = registroOK + "," + i;
                    }
                }
            }
            respuesta.Mensaje = $"Registros Agregados: {registroOK} -- Registros con error: {regitroError}";
            respuesta.Ejecucion = true;
            return respuesta;
        }

        public async Task<Respuesta> CrearCliente(Cliente oCliente)
        {
            Respuesta respuesta = new Respuesta();
            respuesta = await ConsultarCliente(oCliente.ClId); ;
            Cliente cliente = (Cliente)respuesta.Objeto;
            if (cliente != null)
            {
                respuesta.Mensaje = $"La identificación ya esta registrada:{oCliente.ClIdentificacion} ";
                respuesta.Codigo = (int)CodigoError.ErrorGenerico;
            }
            else
            {
                _context.Clientes.Add(oCliente);
                respuesta.Mensaje = $"Registrado correctamente:{oCliente.ClIdentificacion} ";
                Validaciones.AsignarRespuestaOk(respuesta, oCliente);
                await _context.SaveChangesAsync();
            }
            return respuesta;
        }

        public async Task<Respuesta> ConsultarClientes()
        {
            Respuesta respuesta = new Respuesta();
            IEnumerable<Cliente> clientes = await _context.Clientes.ToListAsync();
            Validaciones.AsignarRespuestaOk(respuesta, clientes);
            return respuesta;
        }

        public async Task<Respuesta> EditarCliente(Cliente oCliente)
        {
            Respuesta respuesta = new Respuesta();
            _context.Entry(oCliente).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            Validaciones.AsignarRespuestaOk(respuesta, oCliente);
            return respuesta;
        }

        public async Task<Respuesta> EliminarCliente(int id)
        {
            Respuesta respuesta = new Respuesta();
            var oCliente = await _context.Clientes.FindAsync(id);
            if (oCliente == null)
            {
                respuesta.Mensaje = $"No existe el cliente {id}";
                respuesta.Codigo = (int)CodigoError.ErrorGenerico;
                return respuesta;
            }
            _context.Clientes.Remove(oCliente);
            await _context.SaveChangesAsync();
            respuesta.Mensaje = $"El id: {id} se a eliminado correctamente";
            Validaciones.AsignarRespuestaOk(respuesta, id);
            return respuesta;
        }
    }
}
