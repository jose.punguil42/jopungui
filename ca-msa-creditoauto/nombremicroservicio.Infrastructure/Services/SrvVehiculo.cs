﻿using Microsoft.EntityFrameworkCore;
using nombremicroservicio.Domain.Interfaces;
using nombremicroservicio.Entities.Models;
using nombremicroservicio.Infrastructure.Utilities;
using nombremicroservicio.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static nombremicroservicio.Entities.Utilities.Enumeradores;

namespace nombremicroservicio.Infrastructure.Services
{
    public class SrvVehiculo: IVehiculo
    {
        private readonly PichinchaContext _context;

        public SrvVehiculo(PichinchaContext context)
        {
            _context = context;
        }

        public async Task<Respuesta> ConsultarVehiculos()
        {
            Respuesta respuesta = new Respuesta();
            IEnumerable<Vehiculo> vehiculos = await _context.Vehiculos.ToListAsync();
            Validaciones.AsignarRespuestaOk(respuesta, vehiculos);
            return respuesta;
        }

        public async Task<Respuesta> ConsultarVehiculo(string placa)
        {
            Respuesta respuesta = new Respuesta();
            Vehiculo vehiculo = await _context.Vehiculos.FirstOrDefaultAsync(x => x.VePlaca == placa);
            Validaciones.AsignarRespuestaOk(respuesta, vehiculo);
            return respuesta;
        }

        public async Task<Respuesta> CrearVehiculo(Vehiculo oVehiculo)
        {
            Respuesta respuesta = new Respuesta();
            respuesta = await ConsultarVehiculo(oVehiculo.VePlaca);
            Vehiculo vehiculo = (Vehiculo)respuesta.Objeto;
            SrvMarca srvMarca = new SrvMarca(_context);
            Marca marca = await srvMarca.ConcultarMarcaId(oVehiculo.MaId);
            if (vehiculo != null)
            {
                respuesta.Mensaje = $"La placa ya esta registrada:{oVehiculo.VePlaca}";
            }
            else
            {
                _context.Vehiculos.Add(oVehiculo);
                respuesta.Mensaje = $"Registrado correctamente:{oVehiculo.VePlaca} ";
                Validaciones.AsignarRespuestaOk(respuesta, oVehiculo);
                await _context.SaveChangesAsync();
            }
            return respuesta;
        }

        public async Task<Respuesta> EditarVehiculo(Vehiculo oVehiculo)
        {
            Respuesta respuesta = new Respuesta();
            _context.Entry(oVehiculo).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            Validaciones.AsignarRespuestaOk(respuesta, oVehiculo);
            return respuesta;
        }

        public async Task<Respuesta> EliminarVehiculo(string placa)
        {
            Respuesta respuesta = new Respuesta();
            var oVehiculo = await _context.Vehiculos.FirstOrDefaultAsync(x => x.VePlaca == placa);
            if (oVehiculo == null)
            {
                respuesta.Mensaje = $"No existe el Vehiculo {placa}";
                respuesta.Codigo = (int)CodigoError.ErrorGenerico;
                return respuesta;
            }
            _context.Vehiculos.Remove(oVehiculo);
            await _context.SaveChangesAsync();
            respuesta.Mensaje = $"El Vehiculo con la placa: {placa} se a eliminado correctamente";
            Validaciones.AsignarRespuestaOk(respuesta, placa);
            return respuesta;
        }
    }
}
