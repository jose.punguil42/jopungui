﻿using Microsoft.EntityFrameworkCore;
using nombremicroservicio.Domain.Interfaces;
using nombremicroservicio.Entities.Models;
using nombremicroservicio.Entities.Utilities;
using nombremicroservicio.Infrastructure.Utilities;
using nombremicroservicio.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace nombremicroservicio.Infrastructure.Services
{
    public class SrvSolicitudCredito : ISolicitudCredito
    {
        private readonly PichinchaContext _context;

        public SrvSolicitudCredito(PichinchaContext context)
        {
            _context = context;
        }

        public async Task<Respuesta> ConsultarSolicitudes()
        {
            Respuesta respuesta = new Respuesta();
            IEnumerable<SolicitudCredito> solicitudes = await _context.SolicitudCreditos.ToListAsync();
            Validaciones.AsignarRespuestaOk(respuesta, solicitudes);
            return respuesta;
        }

        public async Task<Respuesta> CrearSolicitudCredito(SolicitudCredito solicitud, Respuesta respuesta)
        {
            await ValidarSolicitudClientePorDia(solicitud, respuesta);
            await ValidarVehiculoReservado(solicitud, respuesta);
            await SolicitudCredito(solicitud, respuesta);
            return respuesta;
        }

        public async Task ValidarSolicitudClientePorDia(SolicitudCredito solicitud, Respuesta respuesta)
        {
            Cliente cliente = await _context.Clientes.FirstOrDefaultAsync(x => x.ClId == solicitud.ClId);
            SolicitudCredito solicitudCredito = await _context.SolicitudCreditos.FirstOrDefaultAsync(x => x.ClId == cliente.ClId 
                && x.ScFechaElaboracion.Value.Date == solicitud.ScFechaElaboracion.Value.Date
                && x.ScEstado == ((int)Enumeradores.EstadoSolicitudCredito.Registrada));
            if(solicitudCredito!= null)
            {
                respuesta.Codigo = (int)Enumeradores.CodigoError.SolicitudIngresada;
                respuesta.Mensaje = 
                    $"El cliente ya solicitó un crédito en día {solicitudCredito.ScFechaElaboracion} -- Estado: " +
                    $"{ UtsEnumerador.ObtenerNombreEnumeradorPorId<Enumeradores.EstadoSolicitudCredito>(solicitudCredito.ScEstado.Value)}";
            }
            Validaciones.ValidarRespuestaOk(respuesta, solicitud);
        }

        public async Task ValidarVehiculoReservado(SolicitudCredito solicitud, Respuesta respuesta)
        {
            Vehiculo vehiculo = await _context.Vehiculos.FirstOrDefaultAsync(x => x.VeId == solicitud.VeId);
            SolicitudCredito solicitudCredito = await _context.SolicitudCreditos.FirstOrDefaultAsync(x => x.VeId == vehiculo.VeId && x.ScEstado == (int)Enumeradores.EstadoSolicitudCredito.Registrada);
            if (solicitudCredito != null)
            {
                respuesta.Codigo = (int)Enumeradores.CodigoError.VehiculoReservado;
                respuesta.Mensaje = $"El auto se encuentra reservado";
            }
            Validaciones.ValidarRespuestaOk(respuesta, solicitud);
        }

        public async Task SolicitudCredito(SolicitudCredito solicitud, Respuesta respuesta)
        {
            solicitud.ScEstado = (int)Enumeradores.EstadoSolicitudCredito.Registrada;
            solicitud.ClFechaAsginacion = solicitud.ScFechaElaboracion;
            solicitud.ClAsignacion = true;
            Validaciones.AsignarRespuestaOk(respuesta);
            _context.SolicitudCreditos.Add(solicitud);
            await _context.SaveChangesAsync();
        }
    }
}
