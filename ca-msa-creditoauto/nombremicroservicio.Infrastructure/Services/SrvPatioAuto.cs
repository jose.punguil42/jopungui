﻿using Microsoft.EntityFrameworkCore;
using nombremicroservicio.Domain.Interfaces;
using nombremicroservicio.Entities.Models;
using nombremicroservicio.Infrastructure.Utilities;
using nombremicroservicio.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace nombremicroservicio.Infrastructure.Services
{
    public class SrvPatioAuto : IPatioAuto
    {
        private readonly PichinchaContext _context;
        public SrvPatioAuto(PichinchaContext context)
        {
            _context = context;
        }

        public async Task<Respuesta> ConsultarPatioAutos()
        {
            Respuesta respuesta = new Respuesta();
            IEnumerable<Patio> patios = await _context.Patios.ToListAsync();
            Validaciones.AsignarRespuestaOk(respuesta, patios);
            return respuesta;
        }

        public async Task<Respuesta> ConsultarPatioAuto(int id)
        {
            Respuesta respuesta = new Respuesta();
            Patio patio = await _context.Patios.FirstOrDefaultAsync(x => x.PaNumeroVenta == id);
            Validaciones.AsignarRespuestaOk(respuesta, patio);
            return respuesta;
        }

        public async Task<Respuesta> CrearPatioAuto(Patio oPatioAuto)
        {
            Respuesta respuesta = new Respuesta();
            _context.Patios.Add(oPatioAuto);
            respuesta.Mensaje = $"Registrado correctamente";
            Validaciones.AsignarRespuestaOk(respuesta, oPatioAuto);
            await _context.SaveChangesAsync();
            return respuesta;
        }

        public async Task<Respuesta> EditarPatioAuto(Patio oPatioAuto)
        {
            Respuesta respuesta = new Respuesta();
            _context.Entry(oPatioAuto).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            Validaciones.AsignarRespuestaOk(respuesta, oPatioAuto);
            return respuesta;
        }

        public async Task<Respuesta> EliminarPatioAuto(int id)
        {
            Respuesta respuesta = new Respuesta();

            var pCuenta = await _context.Patios.FindAsync(id);
            if (pCuenta == null)
            {
                respuesta.Mensaje = $"No existe el PatioAuto {id}";
                return respuesta;
            }
            _context.Patios.Remove(pCuenta);
            await _context.SaveChangesAsync();
            respuesta.Mensaje = $"El id: {id} se a eliminado correctamente";
            Validaciones.AsignarRespuestaOk(respuesta, id);
            return respuesta;
        }

    }
}
