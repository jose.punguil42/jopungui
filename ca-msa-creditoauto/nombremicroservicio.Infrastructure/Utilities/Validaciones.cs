﻿using nombremicroservicio.Entities.Models;
using nombremicroservicio.Entities.UtilException;
using System;
using System.Collections.Generic;
using System.Text;

namespace nombremicroservicio.Infrastructure.Utilities
{
    public static class Validaciones
    {
        public static void ValidarRespuestaOk(Respuesta respuesta)
        {
            ValidarRespuestaOk(respuesta, null);
        }

        public static void ValidarRespuestaOk(Respuesta respuesta, object objeto)
        {
            if (respuesta.Codigo != 100 && respuesta.Codigo != 0 && respuesta.Verificacion != true)
            {
                respuesta.Objeto = objeto;
                throw new CreditoException(respuesta);
            }
        }

        public static void ValidarRespuestaOk(Respuesta respuesta, object objeto, int codigo)
        {
            if (codigo != 100 && codigo != 0)
            {
                respuesta.Codigo = codigo;
                respuesta.Objeto = objeto;
                throw new CreditoException(respuesta);
            }
        }

        public static void AsignarRespuestaOk(Respuesta respuesta, object objeto)
        {
            if (string.IsNullOrEmpty(respuesta.Mensaje))
            {
                respuesta.Mensaje = "OK";
            }
            respuesta.Codigo = 100;
            respuesta.Ejecucion = true;
            respuesta.Objeto = objeto;
        }

        public static void AsignarRespuestaOk(Respuesta respuesta)
        {
            AsignarRespuestaOk(respuesta, null);
        }
    }
}
