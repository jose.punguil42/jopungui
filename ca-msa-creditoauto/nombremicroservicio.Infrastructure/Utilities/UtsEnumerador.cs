﻿using System;
using System.Collections.Generic;
using System.Text;

namespace nombremicroservicio.Infrastructure.Utilities
{
    public static class UtsEnumerador
    {
        public static string ObtenerNombreEnumeradorPorId<T>(int identificador)
        {
            string valor = Enum.GetName(typeof(T), identificador);
            return valor;
        }
    }
}
